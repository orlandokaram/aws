# An introduction to AWS

AWS (Amazon web services) is one of the first and most popular cloud providers; it runs as part of the Amazon infrastructure.

## What is the cloud ?
- Servers not on-premises (other people’s servers)
- Pay per use (CapEx becomes OpEx)
- Almost unlimited capacity
- Fast networks
- Programmable infrastructure
    - Cattle, not pets
    - People manage software, software manages computers
- Many providers
    - AWS (>50% market share, most features)
    - MS Azure
    - Google Cloud Engine
    - Many new and traditional
        - Rackspace
        - IBM, HP, Oracle, RedHat
        - Heroku
        - Digital Ocean

## Kinds of cloud services

- Infrastructure as a service
    - Virtual machines in the cloud (AWS- EC2, now containers, ECS, EKS)
    - Explicit Networking etc (AWS-VPC)
    - 'Unlimited' capacity
    - You still need to manage everything (but have programmability)
- Platform as a service
    - You're given a full platform (DB, app server etc)
    - Heroku, AWS Beanstalk
- Software as a service
    - Software for end-users or developers (APIs), running in the cloud
    - End-users: Google docs, Office 365, Salesforce ...
    - APIs: S3, DynamoDB, Cognito, Auth0 ...

# AWS
AWS provides many services (the console lists about a hundred), with more becoming available every month or so. Getting familiar with all of them can almost be a full-time job, so we will only cover a few (the ones we consider most widely applicable).


The ones we will cover are:
- *[S3](https://aws.amazon.com/s3/)* (Simple Storage Service) - scalable, reliable storage (files)
- *DynamoDB* - Key-Value 'object' database, super-scalable
- *Lambda* - 'serverless' functions, that can run as an API or responding to events
- *CloudFormation* - Create AWS resources with a declarative language.

Other amazon services include:
- *EC2* - Virtual machines in the cloud (will mention only)
- *SQS* - Simple Queue Service 

# Using AWS
You can create AWS resources through:
- the console ([console.aws.amazon.com](https://console.aws.amazon.com))
    - use only/mostly for viewing or exploring
- the cli (command line interface)
    - bash 
    - Powershell
- cloudformation - declarative language, preferred
    - and then use the cli or console to create stacks
- terraform etc

