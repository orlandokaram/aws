# An introduction to git

As programmers, we deal with projects containing hundreds of files, and we modify those files hundreds of times; many times, several programmers are working on the same files; to keep track of all this, we use [Version Control](https://en.wikipedia.org/wiki/Version_control).

One of the most popular version control systems is [Git](https://en.wikipedia.org/wiki/Git). The Git specification defines how to store all the history, and the `git` command is used to modify this history (there are many other commands and graphical tools to view and modify git repositories).

Git stores files, along with the history of all the changes in a *repository*. git repositories can exist in different computers, and can be synchronized with each other.

Although git is designed as a distributed version control system, with no repository being the primary or master, it is many times convenient for us to designate one repository in one machine as the master, and then have every team member synchronize with it. Several companies offer free hosting of git repositories, along wiht web access to the files, and the ability to do code reviews; the most popular is probably [github](https://github.com), but I prefer to use [GitLab](https://gitlab.com), since it provides us with unlimited private repositories.

ToDo
- commits
- branches
- 
